﻿using System;

namespace ReactDotNetAssignment.Api.Models
{
    public class CandidateModel
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string City { get; set; }
        public int? Score { get; set; }
        public bool IsFavourite { get; set; }
    }
}
