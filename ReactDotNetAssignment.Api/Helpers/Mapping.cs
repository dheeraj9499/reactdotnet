﻿using AutoMapper;
using ReactDotNetAssignment.Api.Models;
using ReactDotNetAssignment.EntityDataModel.EntityModel;
using ReactDotNetAssignment.Shared.Functional.DataTransferObjects;

namespace ReactDotNetAssignment.Api.Helpers
{
    public class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CandidateModel, CandidateDTO>();
            CreateMap<CandidateDTO, Candidate>();
            CreateMap<Candidate, CandidateDTO>();
        }
    }
}
