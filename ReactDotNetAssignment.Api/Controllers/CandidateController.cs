﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ReactDotNetAssignment.Api.Models;
using ReactDotNetAssignment.Business.Business;
using ReactDotNetAssignment.Shared.Functional.DataTransferObjects;
using System;
using System.Threading.Tasks;

namespace ReactDotNetAssignment.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class CandidateController : Controller
    {
        private readonly IApplicationBDC _contextBDC;
        private readonly IMapper _mapper;
        public CandidateController(IApplicationBDC contextBDC, IMapper mapper)
        {
            _contextBDC = contextBDC;
            _mapper = mapper;
        }
        [Route("register")]
        [HttpPost]
        public async Task<ActionResult> Register([FromBody] CandidateModel model)
        {
            try
            {
                CandidateDTO candidateDTO = _mapper.Map<CandidateDTO>(model);
                var result = await _contextBDC.RegisterCandidate(candidateDTO);
                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong!");
            }
        }

        [Route("getallcandidates")]
        [HttpGet]
        public ActionResult GetAllCandidates()
        {
            var result = _contextBDC.GetAllCandidates();
            return Ok(result);
        }
        [Route("updatefavmeta")]
        [HttpPut]
        public async Task<ActionResult> UpdateFavMeta(CandidateModel model)
        {
            try
            {
                CandidateDTO candidateDTO = _mapper.Map<CandidateDTO>(model);
                var result = await _contextBDC.UpdateFavMeta(candidateDTO);
                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest("Something Went Wrong!");
            }
        }
        [Route("edit")]
        [HttpPut]
        public async Task<ActionResult> UpdateCandidate(CandidateModel model)
        {
            try
            {
                CandidateDTO candidateDTO = _mapper.Map<CandidateDTO>(model);
                var result = await _contextBDC.UpdateCandidate(candidateDTO);
                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest("Something Went Wrong!");
            }
        }
        [Route("voteup")]
        [HttpPut]
        public async Task<ActionResult> VoteUp(CandidateModel model)
        {
            try
            {
                CandidateDTO candidateDTO = _mapper.Map<CandidateDTO>(model);
                var result = await _contextBDC.VoteUp(candidateDTO);
                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest("Something Went Wrong!");
            }
        }

        [Route("votedown")]
        [HttpPut]
        public async Task<ActionResult> VoteDown(CandidateModel model)
        {
            try
            {
                CandidateDTO candidateDTO = _mapper.Map<CandidateDTO>(model);
                var result = await _contextBDC.VoteDown(candidateDTO);
                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest("Something Went Wrong!");
            }
        }
        [Route("removecandidate/{id:Guid}")]
        [HttpDelete]
        public async Task<ActionResult> RemoveCandidate(Guid id)
        {
            try
            {
                bool isValid = Guid.TryParse(id.ToString(), out Guid Id);
                if (isValid)
                {
                    var result = await _contextBDC.RemoveCandidate(Id);
                    return Ok(result);
                }
                else
                {
                    throw new Exception("Can't parse id");
                }
            }
            catch (Exception)
            {
                return BadRequest("Something Went Wrong!");
            }
        }
        [Route("maxscorers")]
        [HttpGet]
        public ActionResult MaxScorers()
        {
            var result = _contextBDC.MaxScorers();
            return Ok(result);
        }
    }
}