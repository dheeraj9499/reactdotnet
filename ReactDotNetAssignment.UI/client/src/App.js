import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Register from './components/candidate/Register';
import About from './components/About';
import Header from './components/core/header/Header';
import Home from './components/home/Home';
import Edit from './components/candidate/Edit';
import maxScorer from './components/core/analytics/maxScorer';

function App() {
  return (
    <Router>
      <Header />
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/candidate/register" component={Register} />
        <Route path="/candidate/edit" component={Edit} />
        <Route path="/candidate/maxscorers" component={maxScorer} />
        <Route path="/about" component={About} />
      </Switch>
    </Router>
  );
}

export default App;
