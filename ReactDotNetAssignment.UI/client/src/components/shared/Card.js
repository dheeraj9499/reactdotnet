/* eslint-disable react/style-prop-object */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

const cardCustomStyles = {
  display: 'flex',
  flexDirection: 'column',
  gap: '30px',
  width: '350px'
};
const center = {
  padding: '14px',
  fontSize: '18px'
};
const ionIcon = {
  color: 'red'
};

function Card(props) {
  const [candidate, setCandidate] = useState({});
  const history = useHistory();

  useEffect(() => {
    setCandidate(props.candidate);
  }, []);

  const updateFavMeta = async () => {
    const response = await fetch('http://localhost:40632/candidate/updatefavmeta', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify(candidate)
    });
    if (response.ok) {
      const result = await response.json();
      setCandidate(result);
    }
  };
  const voteUp = async () => {
    const response = await fetch('http://localhost:40632/candidate/voteup', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify(candidate)
    });
    if (response.ok) {
      const result = await response.json();
      setCandidate(result);
    }
  };
  const voteDown = async () => {
    const response = await fetch('http://localhost:40632/candidate/votedown', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify(candidate)
    });
    if (response.ok) {
      const result = await response.json();
      setCandidate(result);
    }
  };

  return (
    <>
      <div>
        <div className="card" style={cardCustomStyles}>
          <header className="card-header is-flex is-justify-content-space-between is-align-items-center">
            <span>
              <strong style={center}>{ candidate.name }</strong>
            </span>
            <span>
              <ion-icon to="/edit" name="create-outline" onClick={() => history.push('/candidate/edit', { candidate })} />
              &nbsp; &nbsp;
              <ion-icon name="trash-outline" onClick={() => props.removeCandidate(candidate.id)} />
              &nbsp;
              &nbsp;
            </span>
          </header>
          <div className="card-content">
            <div className="content">
              <div>
                <span>City: </span>
                {candidate.city}
              </div>
              <div>
                <span>Age: </span>
                { candidate.age }
              </div>
              <div>
                <span>Email: </span>
                { candidate.email }
              </div>
              <div>
                <span>Score: </span>
                {candidate.score}
              </div>
            </div>
          </div>
          <footer className="card-footer">
            <button type="button" onClick={updateFavMeta} className="card-footer-item button">{candidate.isFavourite === true ? <ion-icon style={ionIcon} size="large" name="heart" /> : <ion-icon style={ionIcon} size="large" name="heart-outline" />}</button>
            <button type="button" onClick={voteUp} className="card-footer-item button">
              <ion-icon name="chevron-up-outline" />
            </button>
            <button type="button" onClick={voteDown} className="card-footer-item button">
              <ion-icon name="chevron-down-outline" />
            </button>
          </footer>
        </div>
      </div>
    </>
  );
}

export default Card;
