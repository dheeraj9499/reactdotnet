import React, { useEffect } from 'react';
import './Header.css';
import { Link } from 'react-router-dom';

function Header() {
  useEffect(() => {
    const burgerIcon = document.querySelector('#burger');
    const navbarMenu = document.querySelector('#nav-links');
    burgerIcon.addEventListener('click', () => {
      navbarMenu.classList.toggle('is-active');
    });
  }, []);
  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <p className="navbar-item">LOGO</p>
        <span className="navbar-burger" id="burger">
          <span />
          <span />
          <span />
        </span>
      </div>
      <div className="navbar-menu" id="nav-links">
        <div className="navbar-end">
          <Link className="navbar-item" to="/">Home</Link>
          <Link className="navbar-item" to="/candidate/register">Register</Link>
          <Link className="navbar-item" to="/candidate/maxscorers">Max Scorers</Link>
          <Link className="navbar-item" to="/about">About</Link>
          <span>&nbsp;</span>
        </div>
      </div>
    </nav>
  );
}

export default Header;
