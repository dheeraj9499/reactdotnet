import React, { useState, useEffect } from 'react';
import Card from '../../shared/Card';

function maxScorer() {
  const [maxScorers, setMaxscorers] = useState([]);
  const getMaxScorers = async () => {
    const response = await fetch('http://localhost:40632/candidate/maxscorers');
    if (response.ok) {
      const result = await response.json();
      setMaxscorers(result);
    }
  };
  useEffect(() => {
    getMaxScorers();
    return () => {
      console.log('clean component');
    };
  }, []);
  return (
    <div className="container">
      {maxScorers.map((candidate) => (
        <Card
          key={candidate.id}
          candidate={candidate}
        />
      ))}
    </div>
  );
}

export default maxScorer;
