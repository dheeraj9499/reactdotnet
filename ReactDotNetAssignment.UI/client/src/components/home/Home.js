import React, { useEffect, useState } from 'react';
import Card from '../shared/Card';
import './Home.css';

function Home() {
  const [candidates, setCandidates] = useState([]);
  const getCandidates = async () => {
    try {
      const response = await fetch('http://localhost:40632/candidate/getallcandidates');
      if (response.ok) {
        const result = await response.json();
        setCandidates(result);
      }
    } catch {
      console.error('Something went wrong!!!');
    }
  };
  useEffect(() => {
    getCandidates();
    return () => {
      console.log('component destroyed');
    };
  }, []);
  const removeCandidate = async (id) => {
    const wannaDelete = confirm('Sure, you want to delete this candidate?');
    if (wannaDelete) {
      const response = await fetch(`http://localhost:40632/candidate/removeCandidate/${id}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'applicatoin/json; charset=utf-8',
        }
      });
      if (response.ok) {
        getCandidates();
      }
    }
  };
  const editCandidate = async (candidate) => {
    const response = await fetch('http://localhost:40632/candidate/editCandidate', {
      method: 'PUT',
      headers: {
        'Content-Type': 'applicatoin/json; charset=utf-8',
      },
      body: JSON.stringify(candidate)
    });
    if (response.ok) {
      getCandidates();
    }
  };
  return (
    <div className="container">
      {candidates.map((candidate) => (
        <Card
          key={candidate.id}
          candidate={candidate}
          removeCandidate={removeCandidate}
          editCandidate={editCandidate}
        />
      ))}
    </div>
  );
}

export default Home;
