/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
  Formik, Form, Field, ErrorMessage
} from 'formik';
import * as Yup from 'yup';

function Register() {
  const initialValues = {
    name: '',
    age: '',
    email: '',
    mobile: '',
    city: ''
  };

  const onSubmit = async (values) => {
    const response = await fetch('http://localhost:40632/candidate/register', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(values),
      redirect: 'follow'
    });
    if (response.ok) {
      const result = await response.json();
      if (result !== undefined || result !== null) {
        window.location.href = '/';
      }
    }
  };

  const validationSchema = Yup.object({
    name: Yup.string().required(),
    age: Yup.number().required().min(18).max(100),
    email: Yup.string().email().required(),
    mobile: Yup.string().required().length(10),
    city: Yup.string().required()
  });

  const formStyles = {
    display: 'flex',
    padding: '30px',
    gap: '5px',
    width: '350px',
    flexDirection: 'column'
  };
  const formContainer = {
    display: 'grid',
    justifyContent: 'center',
    alignItems: 'center'
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      <div style={formContainer}>
        <Form className="columns is-half is-centered" style={formStyles}>
          <label htmlFor="name">Name</label>
          <Field className="input is-primary" type="text" id="name" name="name" placeholder="Enter your name" />
          <ErrorMessage name="name" />

          <label htmlFor="age">Age</label>
          <Field className="input is-primary" type="number" id="age" name="age" />
          <ErrorMessage name="age" />

          <label htmlFor="email">Email</label>
          <Field className="input is-primary" type="email" id="email" name="email" />
          <ErrorMessage name="email" />

          <label htmlFor="mobile">Mobile</label>
          <Field className="input is-primary" type="text" id="mobile" name="mobile" />
          <ErrorMessage name="mobile" />

          <label htmlFor="city">City</label>
          <Field className="input is-primary" type="text" id="city" name="city" />
          <ErrorMessage name="city" />

          <button className="button is-warning is-small" type="submit">Submit</button>
        </Form>
      </div>
    </Formik>
  );
}

export default Register;
