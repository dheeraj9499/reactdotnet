﻿using Microsoft.Extensions.Logging;
using ReactDotNetAssignment.Business.Validations;
using ReactDotNetAssignment.Data.DataAccessComponents.IRepositories;
using ReactDotNetAssignment.Shared.Functional.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ReactDotNetAssignment.Business.Business
{
    public class ApplicationBDC : IApplicationBDC
    {
        private readonly ICandidateRepository _context;
        private readonly ILogger _logger;
        public ApplicationBDC(ICandidateRepository context, ILogger<ApplicationBDC> logger)
        {
            _context = context;
            _logger = logger;
        }
        public async Task<CandidateDTO> RegisterCandidate(CandidateDTO candidate)
        {
            var validator = new CandidateRegistrationValidator();
            var validationResult = validator.Validate(candidate);
            List<string> errors = new List<string>();
            if (!validationResult.IsValid)
            {
                foreach (var failure in validationResult.Errors)
                {
                    _logger.LogInformation(failure.ErrorMessage);
                    errors.Add(failure.ErrorMessage);
                }
                return null;
            }
            candidate.ProfileCreatedAt = System.DateTime.Now;
            return await _context.RegisterCandidate(candidate);
        }

        public IEnumerable<CandidateDTO> GetAllCandidates()
        {
            var result = _context.GetAllCandidates();
            return result;
        }
        public async Task<CandidateDTO> UpdateFavMeta(CandidateDTO candidate)
        {
            var validator = new CandidateRegistrationValidator();
            var validationResult = validator.Validate(candidate);
            List<string> errors = new List<string>();
            if (!validationResult.IsValid)
            {
                foreach (var failure in validationResult.Errors)
                {
                    _logger.LogInformation(failure.ErrorMessage);
                    errors.Add(failure.ErrorMessage);
                }
                return null;
            }
            candidate.IsFavourite = !candidate.IsFavourite;
            return await _context.UpdateFavMeta(candidate);
        }
        public async Task<CandidateDTO> VoteUp(CandidateDTO candidate)
        {
            var validator = new CandidateRegistrationValidator();
            var validationResult = validator.Validate(candidate);
            List<string> errors = new List<string>();
            if (!validationResult.IsValid)
            {
                foreach (var failure in validationResult.Errors)
                {
                    _logger.LogInformation(failure.ErrorMessage);
                    errors.Add(failure.ErrorMessage);
                }
                return null;
            }
            candidate.Score += 1;
            return await _context.VoteUp(candidate);
        }
        public async Task<CandidateDTO> VoteDown(CandidateDTO candidate)
        {
            var validator = new CandidateRegistrationValidator();
            var validationResult = validator.Validate(candidate);
            List<string> errors = new List<string>();
            if (!validationResult.IsValid)
            {
                foreach (var failure in validationResult.Errors)
                {
                    _logger.LogInformation(failure.ErrorMessage);
                    errors.Add(failure.ErrorMessage);
                }
                return null;
            }
            candidate.Score -= 1;
            return await _context.VoteDown(candidate);
        }
        public async Task<CandidateDTO> UpdateCandidate(CandidateDTO candidate)
        {
            var validator = new CandidateRegistrationValidator();
            var validationResult = validator.Validate(candidate);
            List<string> errors = new List<string>();
            if (!validationResult.IsValid)
            {
                foreach (var failure in validationResult.Errors)
                {
                    _logger.LogInformation(failure.ErrorMessage);
                    errors.Add(failure.ErrorMessage);
                }
                return null;
            }
            return await _context.UpdateCandidate(candidate);
        }
        public async Task<CandidateDTO> RemoveCandidate(Guid Id)
        {
            return await _context.RemoveCandidate(Id);
        }
        public IEnumerable<CandidateDTO> MaxScorers()
        {
            return _context.MaxScorers();
        }

    }
}
