﻿using ReactDotNetAssignment.Shared.Functional.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ReactDotNetAssignment.Business.Business
{
    public interface IApplicationBDC
    {
        public Task<CandidateDTO> RegisterCandidate(CandidateDTO candidate);
        public IEnumerable<CandidateDTO> GetAllCandidates();
        public Task<CandidateDTO> UpdateFavMeta(CandidateDTO candidate);
        public Task<CandidateDTO> VoteUp(CandidateDTO candidate);
        public Task<CandidateDTO> VoteDown(CandidateDTO candidate);
        public Task<CandidateDTO> RemoveCandidate(Guid Id);
        public Task<CandidateDTO> UpdateCandidate(CandidateDTO candidate);
        public IEnumerable<CandidateDTO> MaxScorers();
    }
}
