﻿using FluentValidation;
using ReactDotNetAssignment.Shared.Functional.DataTransferObjects;

namespace ReactDotNetAssignment.Business.Validations
{
    public class CandidateRegistrationValidator : AbstractValidator<CandidateDTO>
    {
        public CandidateRegistrationValidator()
        {
            RuleFor(candidate => candidate.Name)
                .NotNull()
                .MaximumLength(50)
                .MinimumLength(3);
            RuleFor(candidate => candidate.Age)
                .NotNull()
                .GreaterThan(13)
                .LessThan(100);
            RuleFor(candidate => candidate.Email)
                .NotNull()
                .MaximumLength(50)
                .EmailAddress();
            RuleFor(candidate => candidate.Mobile)
                .NotNull()
                .MaximumLength(10)
                .MinimumLength(10);
            RuleFor(candidate => candidate.City)
                .NotNull()
                .MaximumLength(50)
                .MinimumLength(3);
        }
    }
}
