﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReactDotNetAssignment.Shared.Functional.DataTransferObjects
{
    public class CandidateDTO
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string City { get; set; }
        public bool IsFavourite { get; set; }
        public int Score { get; set; }
        public DateTime ProfileCreatedAt { get; set; }
    }
}
