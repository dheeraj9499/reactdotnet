﻿using ReactDotNetAssignment.Data.DataAccessComponents.IRepositories;
using ReactDotNetAssignment.EntityDataModel.EntityModel;
using ReactDotNetAssignment.Shared.Functional.DataTransferObjects;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using AutoMapper;

namespace ReactDotNetAssignment.Data.DataAccessComponents.Repositories
{
    public class CandidateRepository : ICandidateRepository
    {
        private readonly DBContext _context;
        private readonly IMapper _mapper;
        public CandidateRepository(DBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<CandidateDTO> RegisterCandidate(CandidateDTO candidateDTO)
        {
            var candidate = new Candidate
            {
                Id = Guid.NewGuid(),
                Name = candidateDTO.Name,
                Age = candidateDTO.Age,
                Email = candidateDTO.Email,
                Mobile = candidateDTO.Mobile,
                City = candidateDTO.City,
                IsFavourite = false,
                Score = 0,
                ProfileCreatedAt = candidateDTO.ProfileCreatedAt
            };
            _context.Candidates.Add(candidate);
            await _context.SaveChangesAsync();
            return candidateDTO;
        }

        public IEnumerable<CandidateDTO> GetAllCandidates()
        {
            try
            {
                var candidates = _context.Candidates.OrderByDescending(candidate => candidate.ProfileCreatedAt);
                var result = _mapper.Map<IEnumerable<CandidateDTO>>(candidates);
                return result;
            } catch (Exception)
            {
                return null;
            }
        }
        public async Task<CandidateDTO> UpdateFavMeta(CandidateDTO candidateDTO)
        {
            var candidate = new Candidate
            {
                Id = (Guid)candidateDTO.Id,
                Name = candidateDTO.Name,
                Age = candidateDTO.Age,
                Email = candidateDTO.Email,
                Mobile = candidateDTO.Mobile,
                City = candidateDTO.City,
                IsFavourite = candidateDTO.IsFavourite,
            };
            _context.Candidates.Update(candidate);
            await _context.SaveChangesAsync();
            return candidateDTO;
        }
        public async Task<CandidateDTO> VoteUp(CandidateDTO candidateDTO)
        {
            var candidate = new Candidate
            {
                Id = (Guid)candidateDTO.Id,
                Name = candidateDTO.Name,
                Age = candidateDTO.Age,
                Email = candidateDTO.Email,
                Mobile = candidateDTO.Mobile,
                City = candidateDTO.City,
                IsFavourite = candidateDTO.IsFavourite,
                Score = candidateDTO.Score
            };
            _context.Candidates.Update(candidate);
            await _context.SaveChangesAsync();
            return candidateDTO;
        }
        public async Task<CandidateDTO> VoteDown(CandidateDTO candidateDTO)
        {
            var candidate = new Candidate
            {
                Id = (Guid)candidateDTO.Id,
                Name = candidateDTO.Name,
                Age = candidateDTO.Age,
                Email = candidateDTO.Email,
                Mobile = candidateDTO.Mobile,
                City = candidateDTO.City,
                IsFavourite = candidateDTO.IsFavourite,
                Score = candidateDTO.Score
            };
            _context.Candidates.Update(candidate);
            await _context.SaveChangesAsync();
            return candidateDTO;
        }
        public async Task<CandidateDTO> UpdateCandidate(CandidateDTO candidateDTO)
        {
            Candidate candidate = _context.Candidates.Single(can => can.Id == candidateDTO.Id);
            if (candidate != null)
            {
                candidate.Name = candidateDTO.Name;
                candidate.Age = candidateDTO.Age;
                candidate.Email = candidateDTO.Email;
                candidate.Mobile = candidateDTO.Mobile;
                candidate.City = candidateDTO.City;
                _context.Candidates.Update(candidate);
                await _context.SaveChangesAsync();
            }
            return candidateDTO;
        }
        public async Task<CandidateDTO> RemoveCandidate(Guid Id)
        {
            var candidate = _context.Candidates.Where(x => x.Id == Id).Single();
            if (candidate != null)
            {
                _context.Remove(candidate);
                _context.SaveChanges();
            }
            var candidateDto = _mapper.Map<CandidateDTO>(candidate);
            return await Task.FromResult(candidateDto);
        }
        public IEnumerable<CandidateDTO> MaxScorers()
        {
            try
            {
                int score = _context.Candidates.Max(candi => candi.Score);
                var maxScorers = _context.Candidates.Where(candi => candi.Score == score);

                var result = _mapper.Map<IEnumerable<CandidateDTO>>(maxScorers);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
