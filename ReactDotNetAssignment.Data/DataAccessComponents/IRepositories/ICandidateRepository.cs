﻿using ReactDotNetAssignment.Shared.Functional.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ReactDotNetAssignment.Data.DataAccessComponents.IRepositories
{
    public interface ICandidateRepository
    {
        public Task<CandidateDTO> RegisterCandidate(CandidateDTO candidateDTO);
        public Task<CandidateDTO> UpdateFavMeta(CandidateDTO candidateDTO);
        public Task<CandidateDTO> VoteUp(CandidateDTO candidateDTO);
        public Task<CandidateDTO> VoteDown(CandidateDTO candidateDTO);
        public IEnumerable<CandidateDTO> GetAllCandidates();
        public Task<CandidateDTO> RemoveCandidate(Guid Id);
        public Task<CandidateDTO> UpdateCandidate(CandidateDTO candidateDTO);
        public IEnumerable<CandidateDTO> MaxScorers();
    }
}
