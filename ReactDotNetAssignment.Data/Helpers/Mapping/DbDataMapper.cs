﻿using AutoMapper;
using ReactDotNetAssignment.EntityDataModel.EntityModel;
using ReactDotNetAssignment.Shared.Functional.DataTransferObjects;

namespace ReactDotNetAssignment.Data.Helpers.Mapping
{
    public class DbDataMapper : Profile
    {
        public DbDataMapper()
        {
            CreateMap<Candidate, CandidateDTO>();
            CreateMap<CandidateDTO, Candidate>();
        }
    }
}
