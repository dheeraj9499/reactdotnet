# react-dotnet app

## feature

Add Candidates in /register url
Back in home, update their scores and mark it fav/unfav as a heart/heart-fill icon 
Mysql is used as database with EFCore using Dependency Injection.
All of the services in the app and in the data layer has been used as dependency injection.

## Technologies
Frontend: ReactJS (Html, Css, JS)
Backend: .Net Core
Database: MySQL
Tools: Eslint, Entity Framework Core

## Installation
```sh
mkdir project
cd project
git clone https://gitlab.com/dheeraj9499/reactdotnet
cd ReactDotnet
```
or you can clone the project from
https://gitlab.com/dheeraj9499/reactdotnet.git
After entering in project directory open .sol file (Solution file in Visual Studio)

Right click on solution and restore the **nuget** packages
After that, Open the terminal or bash
follow the foloowing commands to install npm packages for **React**
```sh
cd ReactDotNetAssignment.UI/client
```
then run npm i in bash
```sh
npm install
```
After installing the packages for both web api (backend) and react (frontend), now is the time to run the project
1. Run ReactDotNetAssignment.Api in ISS Express

2.  In the client folder run
```sh
npm start
```
Also, Please Change the connection-string which is in appsetting.json in the ReactDotNetAssignment.Api project with yours of mysql.
React and .net core web api will be serving in localhost
👍👍👍
