﻿using Microsoft.EntityFrameworkCore;

namespace ReactDotNetAssignment.EntityDataModel.EntityModel
{
    public class DBContext : DbContext
    {
        public DbSet<Candidate> Candidates { get; set; }
        public DBContext(DbContextOptions<DBContext> options) : base(options) { }
    }
}
