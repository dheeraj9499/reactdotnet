﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ReactDotNetAssignment.EntityDataModel.EntityModel
{
    public class Candidate
    {
        [Key]
        [Required]
        public Guid Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]

        public int Age { get; set; }
        [Required]
        [StringLength(50)]
        public string Email { get; set; }
        [Required]
        [StringLength(10)]
        public string Mobile { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public bool IsFavourite { get; set; }
        [Required]
        public int Score { get; set; }
        [Required]
        public DateTime ProfileCreatedAt { get; set; }
    }
}
